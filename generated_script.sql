-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema Ajax
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Ajax
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Ajax` ;
USE `Ajax` ;

-- -----------------------------------------------------
-- Table `Ajax`.`room`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Ajax`.`room` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `purpose` VARCHAR(100) NULL,
  `access_level` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = ascii;


-- -----------------------------------------------------
-- Table `Ajax`.`zone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Ajax`.`zone` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `access_level` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ajax`.`sensor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Ajax`.`sensor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `zone_id` INT NOT NULL,
  `type` VARCHAR(45) NULL,
  `quantity` INT NULL,
  `status_on` DATETIME NULL,
  `status_off` DATETIME NULL,
  `status_triggered` DATETIME NULL,
  PRIMARY KEY (`id`, `zone_id`),
  INDEX `fk_sensor_zone1_idx` (`zone_id` ASC),
  CONSTRAINT `fk_sensor_zone1`
    FOREIGN KEY (`zone_id`)
    REFERENCES `Ajax`.`zone` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ajax`.`employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Ajax`.`employee` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `employee_info_id` INT NULL DEFAULT NULL,
  `photo` BLOB NULL,
  `first_name` VARCHAR(50) NULL,
  `second_name` VARCHAR(50) NULL,
  `job_status` VARCHAR(50) NULL,
  `shift` DATETIME NULL,
  `employee_id` INT NOT NULL,
  `employee_employee_info_id` INT NOT NULL,
  PRIMARY KEY (`id`, `employee_info_id`),
  INDEX `fk_employee_employee1_idx` (`employee_id` ASC, `employee_employee_info_id` ASC),
  CONSTRAINT `fk_employee_employee1`
    FOREIGN KEY (`employee_id` , `employee_employee_info_id`)
    REFERENCES `Ajax`.`employee` (`id` , `employee_info_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ajax`.`personal__info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Ajax`.`personal__info` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date_of_birth` VARCHAR(45) NULL,
  `phone_number` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `passport_number` VARCHAR(45) NULL,
  `postcode` VARCHAR(45) NULL,
  `city` VARCHAR(45) NULL,
  `street` VARCHAR(45) NULL,
  `number` VARCHAR(45) NULL,
  `employee_id` INT NOT NULL,
  `employee_employee_info_id` INT NOT NULL,
  PRIMARY KEY (`id`, `employee_id`, `employee_employee_info_id`),
  INDEX `fk_personal__info_employee1_idx` (`employee_id` ASC, `employee_employee_info_id` ASC),
  CONSTRAINT `fk_personal__info_employee1`
    FOREIGN KEY (`employee_id` , `employee_employee_info_id`)
    REFERENCES `Ajax`.`employee` (`id` , `employee_info_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ajax`.`notification_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Ajax`.`notification_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(150) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ajax`.`notification`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Ajax`.`notification` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `notification_type_id` INT NOT NULL,
  `sensor_id` INT NOT NULL,
  PRIMARY KEY (`id`, `notification_type_id`, `sensor_id`),
  INDEX `fk_notification_notification_type1_idx` (`notification_type_id` ASC),
  INDEX `fk_notification_sensor1_idx` (`sensor_id` ASC),
  CONSTRAINT `fk_notification_notification_type1`
    FOREIGN KEY (`notification_type_id`)
    REFERENCES `Ajax`.`notification_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_notification_sensor1`
    FOREIGN KEY (`sensor_id`)
    REFERENCES `Ajax`.`sensor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ajax`.`employee_has_zone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Ajax`.`employee_has_zone` (
  `zone_id` INT NOT NULL,
  `employee_id` INT NOT NULL,
  PRIMARY KEY (`zone_id`, `employee_id`),
  INDEX `fk_zone_has_employee_employee1_idx` (`employee_id` ASC),
  INDEX `fk_zone_has_employee_zone1_idx` (`zone_id` ASC),
  CONSTRAINT `fk_zone_has_employee_zone1`
    FOREIGN KEY (`zone_id`)
    REFERENCES `Ajax`.`zone` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_zone_has_employee_employee1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `Ajax`.`employee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ajax`.`zone_has_room`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Ajax`.`zone_has_room` (
  `zone_id` INT NOT NULL,
  `room_id` INT NOT NULL,
  PRIMARY KEY (`zone_id`, `room_id`),
  INDEX `fk_zone_has_room_room1_idx` (`room_id` ASC),
  INDEX `fk_zone_has_room_zone1_idx` (`zone_id` ASC),
  CONSTRAINT `fk_zone_has_room_zone1`
    FOREIGN KEY (`zone_id`)
    REFERENCES `Ajax`.`zone` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_zone_has_room_room1`
    FOREIGN KEY (`room_id`)
    REFERENCES `Ajax`.`room` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
